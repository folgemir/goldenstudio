'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const newer = require('gulp-newer');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const browserSync = require('browser-sync').create();

const del = require('del');

gulp.task('style',function () {
    return gulp.src('development/sass/**/*.sass')
        .pipe(plumber({errorHandler: notify.onError(function (err) {
                return {
                    message: err.message,
                    appID: "123"
                };
            })
        }))
        .pipe(sass())
        .pipe(gulp.dest('public/css'))
});

gulp.task('assets',function () {
    return gulp.src('development/assets/**/*.*')
        .pipe(newer('public'))
        .pipe(gulp.dest('public'))
});

gulp.task('clean',function () {
   return del('public')
});

gulp.task('watch',function () {
    gulp.watch('development/sass/**/*.sass',gulp.series('style'));
    gulp.watch('development/assets/**/*.*',gulp.series('assets'));
});

gulp.task('serve',function () {
    browserSync.init({
        server: 'public'
    });
    browserSync.watch('public/**/*.*').on('change',browserSync.reload);
});

gulp.task('build', gulp.series('clean',gulp.parallel('style','assets')));
gulp.task('dev',gulp.series('build',gulp.parallel('serve','watch')));
